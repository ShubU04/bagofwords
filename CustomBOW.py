from classifier import Classifier


class ShubUBagofWord:
    def __init__(self):
        self.dictionary = {}
        self.bagofwords = None

    def addDict(self, sentence, classifier):
        if self.dictionary.get(classifier) == None:
            self.dictionary[classifier] = [sentence]
        else:
            self.dictionary[classifier].append(sentence)

    def listToDict(self, keyFunction,valueFunction,  values):
        return dict((keyFunction(v), valueFunction(v)) for v in values)

    def transform(self):
        for key,value in self.dictionary.iteritems():
            for sentence in value:
                words = sentence.split(" ")
                listDict = map(lambda word: {word: words.count(word)}, words)
                self.dictionary[key] = self.listToDict(lambda a: a.keys()[0], lambda b: b[b.keys()[0]], listDict)
        print(self.dictionary)

    def toString(self):
        return self.bagofwords

    def getBag(self):
        return self.bagofwords






bag = ShubUBagofWord()
classifier = Classifier()

bag.addDict("just plain boring", "-")
bag.addDict("just plain boring", "-")
bag.transform()

#classifier.inp(bag.getBag(), 2)