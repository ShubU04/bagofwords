import math
import numpy as np
from inputReader import InputReader
from tqdm import tqdm

class Classifier:

# classifier ['+','-']
# input document have to pass pre optimized process
# doc input { classifier : [String] }

    def train(self, document, classifiers):
        vocabList = self.getVocab(document)
        logPrior = {classifier: float(len(document[classifier])) / self.getNumTrainSet(document) for classifier in classifiers}
        loglikelihood = {}
        for classifier in tqdm(classifiers):
            bigDocC = self.getTrainSet(document, classifier)
            total = len(bigDocC) + len(vocabList)
            loglikelihood[classifier] = {word: float(self.getOccurence(word, bigDocC) + 1) / total for word in vocabList}

        return logPrior, loglikelihood, vocabList

    def test(self, testDocument, logprior, loglikelihood, classifiers, vocabulary):
        print("================================= testing ====================================================")
        sum = {}
        result = []
        for i in testDocument:
            sentence = i.split(" ")
            check = []
            for classifier in classifiers:
                sum[classifier] = logprior[classifier]
                for word in sentence:
                    if word in vocabulary:
                        temp = loglikelihood[classifier]
                        sum[classifier] *= temp[word]
                check.append(sum[classifier])

            result.append({ i : np.argmax(check) })

        print(result)
        print(classifiers)
        return np.argmax(result)

    def getOccurence(self, word, array):
        return array.count(word)

    def getNumTrainSet(self, document):
        return len(reduce(lambda x, y: x + y, map(lambda kv: kv[1], document.iteritems())))

    def getTrainSet(self, document, classifier):
        return reduce(lambda v1, v2: (v1 + " " + v2), document[classifier]).split(" ")

    def getVocab(self, document):
        temp = []
        for key,value in document.iteritems():
            for sentence in value:
                temp += filter(lambda x: x not in ["the", "a", "an", "of", "and"], list(set(sentence.split(" "))))
        return temp


c = Classifier()
classifier = ['0','1']

reader = InputReader("trainData.txt")

logprior,loglikelihood,vocabList = c.train(reader.readTrainData(),classifier)
print(logprior)
print(loglikelihood['1'])
print(loglikelihood['0'])
testDocument = reader.readTestData("testData.txt")

print(c.test(testDocument, logprior, loglikelihood, classifier, vocabList))

