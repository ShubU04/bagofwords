
import string

class InputReader:
    def __init__(self, filePath):
        self.filePath = filePath
        self.file = open(filePath, 'r')

    def readTrainData(self):
        dictionary = dict()

        for line in self.file.readlines():
            if line[0] in dictionary.keys():
                dictionary[line[0]].append(line[1:].translate(None, string.punctuation).translate(None, "\t\n"))
            else:
                dictionary[line[0]] = [line[1:].translate(None, string.punctuation).translate(None, "\t\n")]

        return dictionary

    def readTestData(self, path):
        file = open(path, 'r')
        inp = []
        for line in file.readlines():
            inp.append(line.translate(None, string.punctuation).translate(None, "\t\n"))

        return inp




