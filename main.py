
from sklearn.feature_extraction.text import CountVectorizer
from CustomBOW import ShubUBagofWord


class BagofWords:
    def __init__(self):
        self.vectorizor = CountVectorizer()
        self.dictionary = []
        self.bagofwords = None

    def addDict(self, sentence):
        self.dictionary.append(sentence)

    def generate(self):
        bagFit = self.vectorizor.fit(self.dictionary)
        self.bagofwords = self.vectorizor.transform(self.dictionary)

    def toString(self):
        return str(self.bagofwords)

    def indexAt(self, word):
        return self.vectorizor.vocabulary_.get(word)

#
# bag = BagofWords()
# bag.addDict("test one two three four")
# bag.addDict("this is the test again test")
# bag.generate()
# print(bag.toString())
# print(bag.indexAt("test"))
# print(bag.indexAt("one"))









